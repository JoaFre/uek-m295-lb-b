const express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
const app = express();
const port = 3000;

var jsonParser = bodyParser.json()

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require("../config/swagger.json");

app.use(session({
    secret: 'verySecret',
    resave: false,
    saveUninitialized: false
}));

password = "m295"

app.post('/login', async (request, response) => { //Log In Funktion
    try {
        if (request.headers.authorization.split(/:(.*)/s)[1] == password) {
            request.session.email = request.headers.authorization.split(/:(.*)/s)[0] //Spaltete das erste ':' um E-Mail und Password zu trennen.
            request.session.authorized = true
            response.status(201)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
            })
        }
        response.status(401)
        response.contentType('json')
        return response.send({
            "loggedInWith": request.session.email || null,
            "authorized": request.session.authorized || false,
            "error": "Not logged in. Please use /login POST to authenticate yourself."
        })
    } catch (error) {
        response.sendStatus(500)
    }
});

app.get('/verify', async (request, response) => {// Kontrolliert die Session in dem sie die Variabeln darin anschaut.
    try {
        if (request.session.authorized) {
            response.status(200)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
            })
        }
        response.status(200)
        response.contentType('json')
        return response.send({
            "loggedInWith": request.session.email || null,
            "authorized": request.session.authorized || false,
            "error": "Not logged in. Please use /login POST to autheticate yourself."
        })
    } catch (error) {
        response.sendStatus(500)
    }
});

app.delete('/logout', async (request, response) => {// Loggt die Session aus in dem sie sie zerstört.
    try {
        request.session.destroy()
        return response.sendStatus(204)
    } catch (error) {
        response.sendStatus(500)
    }
});


tasks = { //Liste der Tasks. Tasks sind als JSON gespeichert. Die ID ist sowohl der Property der den Task enthaltet, sowie ein Property des Tasks. Kann zu Problemen führen, aber macht suchen von IDs sehr einfach.
    1: {
        "id": 1,
        "title": "Task Titel 1",
        "desc": "Beschreibung von Task 1",
        "done": false,
        "dueDate": new Date('2023-07-01')
    }
}

autoIncrement = 2;//Da Task 1 schon existiert ist es 2.

app.get('/tasks', async (request, response) => {//Gibt aus Tasks.
    try {
        if (!request.session.authorized) {
            response.status(401)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
                "error": "Not logged in. Please use /login POST to autheticate yourself."
            })
        }
        response.contentType('json')
        response.status(200)
        return response.send(tasks)
    } catch (error) {
        response.sendStatus(500)
    }
});

app.get('/tasks/:id', async (request, response) => {//Gibt aus Task mit der angegebenen ID
    try {
        if (!request.session.authorized) {
            response.status(401)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
                "error": "Not logged in. Please use /login POST to autheticate yourself."
            })
        }
        const id = request.params.id;
        if (id in tasks) {
            response.contentType('json')
            response.status(200)
            return response.send(tasks[id])
        }
        response.status(404)
        return response.send({
            "error": "requested id does not exist."
        })
    } catch (error) {
        response.sendStatus(500)
    }
});

app.post('/tasks', jsonParser, async (request, response) => {// Erstellt neuen Task.
    try {
        if (!request.session.authorized) {
            response.status(401)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
                "error": "Not logged in. Please use /login POST to autheticate yourself."
            })
        }
        let newTask = request.body
        if (typeof newTask.title == "undefined" || typeof newTask.desc == "undefined" || typeof newTask.done == "undefined" || typeof newTask.dueDate == "undefined") {// Dies stellt sicher das alle nötige Properties existieren
            response.status(422)
            return response.send({
                "error": "Invalid Content. Missing Properties."
            })
        }
        newTask["id"] = autoIncrement
        autoIncrement++
        tasks[newTask.id] = {// Dies stellt sicher das keine falschen Properties eingeschmuggelt werden können.
            "id": newTask.id,
            "title": newTask.title,
            "desc": newTask.desc,
            "done": newTask.done,
            "dueDate": new Date(newTask.dueDate)
        }
        response.contentType('json')
        response.status(201)
        return response.send(tasks[newTask.id])
    } catch (error) {
        response.sendStatus(500)
    }
});

app.put('/tasks/:id', jsonParser, async (request, response) => {
    try {
        if (!request.session.authorized) {
            response.status(401)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
                "error": "Not logged in. Please use /login POST to autheticate yourself."
            })
        }
        const id = request.params.id;
        if (id in tasks) {
            let newTask = request.body
            if (typeof newTask.title == "undefined" || typeof newTask.desc == "undefined" || typeof newTask.done == "undefined" || typeof newTask.dueDate == "undefined") {// Dies stellt sicher das alle nötige Properties existieren
                response.status(422)
                return response.send({
                    "error": "Invalid Content. Missing Properties."
                })
            }
            delete tasks[id]
            tasks[id] = {// Dies stellt sicher das keine falschen Properties eingeschmuggelt werden können.
                "title": newTask.title,
                "desc": newTask.desc,
                "done": newTask.done,
                "dueDate": newTask.dueDate
            }
            response.contentType('json')
            response.status(200)
            return response.send(tasks[id])
        }
        response.status(404)
        return response.send({
            "error": "requested id does not exist."
        })
    } catch (error) {
        response.sendStatus(500)
    }
});

app.delete('/tasks/:id', async (request, response) => {
    try {
        if (!request.session.authorized) {
            response.status(401)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
                "error": "Not logged in. Please use /login POST to autheticate yourself."
            })
        }
        const id = request.params.id;
        if (id in tasks) {
            let deletedTask = tasks[id]
            delete tasks[id];
            response.status(200)
            return response.send(deletedTask)
        }
        response.status(404)
        return response.send({
            "error": "requested id does not exist."
        })
    } catch (error) {
        response.sendStatus(500)
    }
});

app.patch('/tasks/:id', jsonParser, async (request, response) => {
    try {
        if (!request.session.authorized) {
            response.status(401)
            response.contentType('json')
            return response.send({
                "loggedInWith": request.session.email || null,
                "authorized": request.session.authorized || false,
                "error": "Not logged in. Please use /login POST to autheticate yourself."
            })
        }
        const id = request.params.id;
        if (id in tasks) {
            let newTask = request.body
            if (typeof newTask.title == "undefined" && typeof newTask.desc == "undefined" && typeof newTask.done == "undefined" && typeof newTask.dueDate == "undefined") {// Dies stellt sicher das es brauchbare Properties gibt.
                response.status(422)
                return response.send({
                    "error": "Invalid Content. Missing All Properties."
                })
            }
            if (typeof newTask.title == "undefined") newTask.title = tasks[id].title;// Dies stellt sicher das keine falschen Properties eingeschmuggelt werden können.
            if (typeof newTask.desc == "undefined") newTask.desc = tasks[id].desc;
            if (typeof newTask.done == "undefined") newTask.done = tasks[id].done;
            if (typeof newTask.dueDate == "undefined") newTask.dueDate = tasks[id].dueDate;
            delete tasks[id]
            tasks[id] = {
                "title": newTask.title,
                "desc": newTask.desc,
                "done": newTask.done,
                "dueDate": newTask.dueDate
            }
            response.contentType('json')
            response.status(200)
            return response.send(tasks[id])
        }
        return response.sendStatus(404)
    } catch (error) {
        response.sendStatus(500)
    }
});

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));//Die ist verantwortlich für die Swagger Page

app.all('*', async (request, response) => {//Dies gibt die 404 Seite aus.
    try {
        response.status(404)
        response.send({
            "error": "Invalid Request. Ressource not found."
        })
    } catch (error) {
        response.sendStatus(500)
    }
})

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});