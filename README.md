# Projekt LB-B für das Modul 295

von Joa Frehner

Dies ist ein Projekt für das ÜK Modul 295; Backend für Applikationen realisieren.

In diesem Projekt wird eine simple API erstellt welche Tasks bearbeitet.

Dieses Projekt ist ein Node.js Server der mit Express und einigen anderen Packages funktioniert.

Für Node.js v22.0.0 geschrieben.

## Packages:

* express
  
  * express-session
  
  * body-parser
  
  * swagger-ui-express

##### Verwendet, aber nicht nötig zum laufen:

* swagger-autogen

### Beschreibung

In dieser API werden Task Objekte gespeichert und manipuliert. Die API basiert auf REST-Prinzipien.

Es werden CRUD-Operation implementiert welche sich auf die Tasks beziehen. Des weiteren wurde ein simples Authorisierungs-System implementiert. Diese System ist natürlicherweise nicht sicher, da die Verbindung, wie auch die Passwörter, nicht versteckt oder sicher sind, aber es dient trotzdem zur Demonstration des Konzeptes.

### Dokumentation

Diese beiden Systeme sind in der Swagger Dokumentation unter /swagger dokumentiert. Alle Endpunkte und deren Funktion und Verhalten ist dort beschrieben.

Beschreibungen der Endpunkte, sowie deren erwarteten Werte sind dort beschrieben.

[Joa Frehner / ÜK-M295-LB-B · GitLab](https://gitlab.com/JoaFre/uek-m295-lb-b)

### Testing

Des Weiteren ist eine Postman Collection enthalten, welche mit verschiedenen Requests testet, ob die 9 Endpoints der API funktionieren. Diese Datei befindet sich ebenfalls im Dokumentation Ordner.
